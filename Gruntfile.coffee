files = [
	"src/init.coffee"
	"src/RandomUtil.coffee"
]

module.exports = (grunt) ->
	grunt.initConfig
		pkg: grunt.file.readJSON "package.json"

		coffee:
			# Compiles all files to check for compilation errors.
			all:
				expand: true
				cwd: ""
				src: ["src/**/*.coffee", "spec/**/*.coffee"]
				dest: "build/"
				ext: ".js"

			# Compiles the framework into a single JS file.
			framework:
				files:
					"build/random-util.js": files
				options:
					join: true

		jessie:
			all:
				expand: true
				cwd: ""
				src: "build/spec/**/*.js"

		coffeelint:
			src:
				files:
					src: files
			options: require "./coffeelint.coffee"

		concat:
			# Packages the final JS file with a header
			dist:
				options:
					banner: "/*! <%= pkg.fullname %> <%= pkg.version %> - MIT license */\n"
					process: true
				src: ["build/random-util.js"]
				dest: "build/random-util.js"

		copy:
			# Copies the built dist file to the root for npm packaging
			dist:
				files:
					"random-util.js": "build/random-util.js"

		watch:
			files: ["src/**", "spec/**"]
			tasks: "default"

	grunt.loadNpmTasks "grunt-contrib-coffee"
	grunt.loadNpmTasks "grunt-contrib-concat"
	grunt.loadNpmTasks "grunt-contrib-copy"
	grunt.loadNpmTasks "grunt-contrib-watch"
	grunt.loadNpmTasks "grunt-jessie"
	grunt.loadNpmTasks "grunt-coffeelint"

	grunt.registerTask "default", [
		"coffee:all"
		"coffee:framework"
		"concat:dist"
		"copy:dist"
		"jessie"
		"coffeelint"
	]
