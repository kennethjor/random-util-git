A collection of functions for dealing with random numbers.

## Uniform random numbers

* `random()`
* `randomInteger(min, max)`
* `randomNumber(min, max)`

# Biased random numbers

* `boased(bias)`
* `biasedInteger(min, max, bias)`
* `biasedNumber(min, max, bias)`
