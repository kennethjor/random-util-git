/*! RandomUtil 0.0.5 - MIT license */
(function() {
  var RandomUtil, abs, exports, floor, pow, root,
    __hasProp = {}.hasOwnProperty;

  root = this;

  RandomUtil = {
    version: "0.0.5"
  };

  if (typeof exports !== "undefined") {
    exports = RandomUtil;
  } else if (typeof module !== "undefined" && module.exports) {
    module.exports = RandomUtil;
  } else if (typeof define === "function" && define.amd) {
    define([], RandomUtil);
  } else {
    root["RandomUtil"] = RandomUtil;
  }

  floor = Math.floor, pow = Math.pow, abs = Math.abs;

  RandomUtil.random = Math.random;

  RandomUtil.randomInt = function(min, max) {
    return floor(this.random() * (max + 1 - min) + min);
  };

  RandomUtil.randomNumber = function(min, max) {
    return this.random() * (max - min) + min;
  };

  RandomUtil.arrayElement = function(arr) {
    var index;
    index = this.randomInt(0, arr.length - 1);
    return arr[index];
  };

  RandomUtil.biased = function(bias) {
    var r;
    r = this.random();
    if (bias === 0) {
      return r;
    }
    if (bias < 0) {
      return pow(r, -bias + 1);
    }
    if (bias > 0) {
      return 1 - pow(1 - r, bias + 1);
    }
  };

  RandomUtil.biasedInt = function(min, max, bias) {
    return floor(this.biased(bias) * (max + 1 - min) + min);
  };

  RandomUtil.biasedNumber = function(min, max, bias) {
    return this.biased() * (max - min) + min;
  };

  RandomUtil.weighted = function(obj) {
    var current, i, pos, r, sum, v, x;
    sum = 0;
    for (i in obj) {
      if (!__hasProp.call(obj, i)) continue;
      v = obj[i];
      x = parseFloat(v);
      if (isNaN(x) || (!isFinite(x)) || x < 0) {
        throw new Error("Invalid weight value \"" + v + "\" [" + (typeof v) + "] for index \"" + i + "\"");
      }
      sum += x;
    }
    if (sum === 0) {
      return void 0;
    }
    r = this.random();
    pos = r * sum;
    current = 0;
    for (i in obj) {
      if (!__hasProp.call(obj, i)) continue;
      v = obj[i];
      current += parseFloat(v);
      if (current > pos) {
        return i;
      }
    }
    return false;
  };

  RandomUtil.integerArrayWithSum = function(n, sum) {
    var arr, i, _i, _j, _ref, _ref1;
    arr = [];
    for (i = _i = 0, _ref = n - 2; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
      arr[i] = this.randomInt(0, sum);
    }
    arr[n - 1] = sum;
    arr = arr.sort(function(a, b) {
      return a - b;
    });
    for (i = _j = _ref1 = n - 1; _ref1 <= 1 ? _j <= 1 : _j >= 1; i = _ref1 <= 1 ? ++_j : --_j) {
      arr[i] -= arr[i - 1];
    }
    return arr;
  };

}).call(this);
