_ = require "underscore"
sinon = require "sinon"

RandomUtil = require "../random-util"

# Custom matcher to check for uniform arrays.
toBeUniformlyDistributed = () ->
	arr = @actual
	# Calc sum.
	sum = 0
	for own i, v of arr
		sum += v
	# Calc share.
	share = []
	for own i, v of arr
		share.push arr[i] / sum
	# Calc error.
	error = 0
	for v in share
		error += Math.pow( v - 1 / share.length , 2 )
	error /= share.length
	if _.isNaN error
		console.log arr, error
		throw new Error "Failed to calculate error"
	if error > 0.01
		notText = if @isNot then "not" else ""
		@message = do (arr, error, notText) -> -> "Expected #{jasmine.pp arr} #{notText}to be uniformly distributed. Error was #{error}"
		return false
	return true


describe "RandomUtil", ->
	state = null
	precision = null
	increment = null

	beforeEach ->
		@addMatchers toBeUniformlyDistributed: toBeUniformlyDistributed
		precision = 4
		state = 0.0
		increment = 0.1
		RandomUtil.random = ->
			r = state
			state += increment
			if r <= 0.0 then return 0.0000001
			if r >= 1.0 then return 0.9999999
			return r

	it "should generate predictable random numbers during tests", ->
		expect(RandomUtil.random()).toBeCloseTo 0.0, precision
		expect(RandomUtil.random()).toBeCloseTo 0.1, precision
		expect(RandomUtil.random()).toBeCloseTo 0.2, precision
		expect(RandomUtil.random()).toBeCloseTo 0.3, precision

	it "should generate random integers", ->
		expect(RandomUtil.randomInt 3, 12).toEqual 3
		expect(RandomUtil.randomInt 3, 12).toEqual 4
		expect(RandomUtil.randomInt 3, 12).toEqual 5
		expect(RandomUtil.randomInt 3, 12).toEqual 6
		expect(RandomUtil.randomInt 3, 12).toEqual 7
		expect(RandomUtil.randomInt 3, 12).toEqual 8
		expect(RandomUtil.randomInt 3, 12).toEqual 9
		expect(RandomUtil.randomInt 3, 12).toEqual 10
		expect(RandomUtil.randomInt 3, 12).toEqual 11
		expect(RandomUtil.randomInt 3, 12).toEqual 12

	it "should generate evenly distributed random integers", ->
		RandomUtil.random = Math.random
		generated = {}
		n = 1000000
		for i in [1..n]
			v = RandomUtil.randomInt 5, 9
			generated[v] or= 0
			generated[v]++
		generated[8] *= 2
		expect(generated).toBeUniformlyDistributed()

	it "should generate random numbers", ->
		expect(RandomUtil.randomNumber 10, 15).toBeCloseTo 10.0, precision
		expect(RandomUtil.randomNumber 10, 15).toBeCloseTo 10.5, precision
		expect(RandomUtil.randomNumber 10, 15).toBeCloseTo 11.0, precision
		expect(RandomUtil.randomNumber 10, 15).toBeCloseTo 11.5, precision
		expect(RandomUtil.randomNumber 10, 15).toBeCloseTo 12.0, precision
		expect(RandomUtil.randomNumber 10, 15).toBeCloseTo 12.5, precision
		expect(RandomUtil.randomNumber 10, 15).toBeCloseTo 13.0, precision
		expect(RandomUtil.randomNumber 10, 15).toBeCloseTo 13.5, precision
		expect(RandomUtil.randomNumber 10, 15).toBeCloseTo 14.0, precision
		expect(RandomUtil.randomNumber 10, 15).toBeCloseTo 14.5, precision
		expect(RandomUtil.randomNumber 10, 15).toBeCloseTo 15.0, precision

	it "should generate biased randoms", ->
		increment = 0
		state = 0.5
		expect(RandomUtil.biased 0).toBeCloseTo 0.5, precision
		expect(RandomUtil.biased -1).toBeCloseTo 0.25, precision
		expect(RandomUtil.biased 1).toBeCloseTo 0.75, precision
		expect(RandomUtil.biased -2).toBeCloseTo 0.125, precision
		expect(RandomUtil.biased 2).toBeCloseTo 0.875, precision
		state = 0.1
		expect(RandomUtil.biased -1).toBeCloseTo 0.01, precision
		expect(RandomUtil.biased -2).toBeCloseTo 0.001, precision
		expect(RandomUtil.biased -3).toBeCloseTo 0.0001, precision
		state = 0.9
		expect(RandomUtil.biased 1).toBeCloseTo 0.99, precision
		expect(RandomUtil.biased 2).toBeCloseTo 0.999, precision
		expect(RandomUtil.biased 3).toBeCloseTo 0.9999, precision

	it "should generate biased random integers", ->
		state = 0.0
		increment = 0.25
		expect(RandomUtil.biasedInt 5, 15, -2).toBe 5
		expect(RandomUtil.biasedInt 5, 15, -2).toBe 5
		expect(RandomUtil.biasedInt 5, 15, -2).toBe 6
		expect(RandomUtil.biasedInt 5, 15, -2).toBe 9
		expect(RandomUtil.biasedInt 5, 15, -2).toBe 15
		state = 0.0
		increment = 0.25
		expect(RandomUtil.biasedInt 5, 15, 2).toBe 5
		expect(RandomUtil.biasedInt 5, 15, 2).toBe 11
		expect(RandomUtil.biasedInt 5, 15, 2).toBe 14
		expect(RandomUtil.biasedInt 5, 15, 2).toBe 15
		#expect(RandomUtil.biasedInt 5, 15, 2).toBe 15

	it "should generate biased random numbers"

	it "should select random array elements", ->
		increment = 0.25
		arr = "abcd".split("")
		expect(RandomUtil.arrayElement arr).toBe "a"
		expect(RandomUtil.arrayElement arr).toBe "b"
		expect(RandomUtil.arrayElement arr).toBe "c"
		expect(RandomUtil.arrayElement arr).toBe "d"

	describe "weighted hash", ->
		it "should select based on weights", ->
			increment = 1/(6-1)
			obj =
				a: 0
				b: 1
				c: 2
				d: 0
				e: 3
			expect(RandomUtil.weighted obj).toBe "b"
			expect(RandomUtil.weighted obj).toBe "c"
			expect(RandomUtil.weighted obj).toBe "c"
			expect(RandomUtil.weighted obj).toBe "e"
			expect(RandomUtil.weighted obj).toBe "e"
			expect(RandomUtil.weighted obj).toBe "e"

		it "should ignore empty objects", ->
			expect(RandomUtil.weighted {}).toBe undefined
			expect(RandomUtil.weighted {a:0}).toBe undefined
			expect(RandomUtil.weighted {a:0,b:0}).toBe undefined

		it "should complain about invalid weights", ->
			expect(-> RandomUtil.weighted {a:NaN}).toThrow("Invalid weight value \"NaN\" [number] for index \"a\"")
			expect(-> RandomUtil.weighted {a:Infinity}).toThrow("Invalid weight value \"Infinity\" [number] for index \"a\"")
			expect(-> RandomUtil.weighted {a:-Infinity}).toThrow("Invalid weight value \"-Infinity\" [number] for index \"a\"")
			expect(-> RandomUtil.weighted {a:-1}).toThrow("Invalid weight value \"-1\" [number] for index \"a\"")
			expect(-> RandomUtil.weighted {a:"x"}).toThrow("Invalid weight value \"x\" [string] for index \"a\"")
			expect(-> RandomUtil.weighted {a:null}).toThrow("Invalid weight value \"null\" [object] for index \"a\"")
			expect(-> RandomUtil.weighted {a:undefined}).toThrow("Invalid weight value \"undefined\" [undefined] for index \"a\"")

	it "should generate a random array of integers which sum to a constant", ->
		RandomUtil.random = Math.random
		n = 1000
		sum = 20
		vals = 5
		uniform = []
		for i in [0..vals - 1]
			uniform[i] = 0
		for i in [1..n]
			# Generate.
			arr = RandomUtil.integerArrayWithSum vals, sum
			# Check count.
			expect(arr.length).toBe 5
			# Check sum and constraints.
			s = 0
			for i in arr
				expect(i).toBeGreaterThan -1
				expect(i).toBeLessThan sum + 1
				s += i
			expect(s).toBe sum
			# Add to uniform check.
			for i in [0..vals - 1]
				uniform[i] += arr[i]
		expect(uniform).toBeUniformlyDistributed()

