{floor, pow, abs} = Math

# Returns a random number between 0 and 1.
# Replace this method to specify a custom random generator.
RandomUtil.random = Math.random

# Returns a random integer between `min` and `max`, inclusive.
RandomUtil.randomInt = (min, max) ->
	return floor( @random() * (max + 1 - min) + min )

# Returns a random number between `min` and `max`.
RandomUtil.randomNumber = (min, max) ->
	return @random() * (max - min) + min

# Returns a random element from an array.
RandomUtil.arrayElement = (arr) ->
	index = @randomInt 0, arr.length - 1
	return arr[index]

# Returns a biased random number.
# `bias` is a measure of much towards the extremes the generation should be.
# Negative bias will go towards 0, while a positive bias will go towards 1.
RandomUtil.biased = (bias) ->
	r = @random()
	if bias is 0
		return r
	if bias < 0
		return pow r, -bias + 1
	if bias > 0
		return 1 - pow 1 - r, bias + 1

# Returns a random biased integer between `min` and `max`, inclusive.
RandomUtil.biasedInt = (min, max, bias) ->
	return floor( @biased(bias) * (max + 1 - min) + min )

# Returns a random biased number between `min` and `max`.
RandomUtil.biasedNumber = (min, max, bias) ->
	return @biased() * (max - min) + min

# Given an object with string indexes and integer weights, will return a random index based on those weights.
#
#     var val = RandomUtil({
#        "bird": 2
#        "dog": 1
#        "cat": 0
#     });
#
# Bird is now twice as likely to be returned as dog, and cat will never be returned.
RandomUtil.weighted = (obj) ->
	sum = 0
	for own i, v of obj
		x = parseFloat v
		if isNaN(x) or (not isFinite(x)) or x < 0
			throw new Error "Invalid weight value \"#{v}\" [#{typeof v}] for index \"#{i}\""
		sum += x
	return undefined if sum is 0
	r = @random()
	pos = r * sum
	current = 0
	for own i, v of obj
		current += parseFloat v
		if current > pos
			return i
	return false

# Creates an array with `n` elements such that the sum of all elements equals `sum`.
# Code based on `ojr_array_with_sum` from `ojrandlib`.
RandomUtil.integerArrayWithSum = (n, sum) ->
	arr = []
	for i in [0..n - 2]
		arr[i] = @randomInt 0, sum
	arr[n - 1] = sum
	arr = arr.sort (a, b) -> return a - b
	for i in [n - 1..1]
		arr[i] -= arr[i - 1]
	return arr
