# Initialises the global object and ties into whatever loader is present.

root = this

# Init main object.
RandomUtil = version: "<%= pkg.version %>"

# CommonJS
if typeof exports isnt "undefined"
	exports = RandomUtil
else if typeof module isnt "undefined" and module.exports
	module.exports = RandomUtil
# AMD
else if typeof define is "function" and define.amd
	define [], RandomUtil
# Browser
else
	root["RandomUtil"] = RandomUtil
